package com.stolbunov.roman.module_2.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.module_2.R;
import com.stolbunov.roman.module_2.data.cash.RuntimeCash;
import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FighterType;
import com.stolbunov.roman.module_2.data.model.fighters.FightersFactory;

public class ChoiceFighterCharacteristicsActivity extends AppCompatActivity {
    private ImageView fighterImage;
    private TextView fighterName;
    private TextView fighterClass;
    private TextView fighterDescription;
    private TextView fighterHP;
    private TextView fighterArmor;
    private TextView fighterDamage;

    private RuntimeCash cash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_fighter_characteristics);

        Intent intent = getIntent();
        creatingNewFighterOfChoice(intent);

        initViews();
        fillingTitleView();
        restoreState(savedInstanceState);
    }

    private void creatingNewFighterOfChoice(Intent intent) {
        String nameType = intent.getStringExtra(ChoiceFighterActivity.SELECTED_FIGHTER);
        ArenaFighter fighter = FightersFactory.generateFighter(FighterType.valueOf(nameType));
        cash = RuntimeCash.getInstance();
        cash.setCurrentFighter(fighter);
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            ArenaFighter fighter = cash.getCurrentFighter();
            displayNewFighterParameters(fighter);
        } else {
            displayNewFighterParameters(createNewFighterParams());
        }
    }

    private void fillingTitleView() {
        String fighterType = cash.getCurrentFighter().getFighterClass();
        String descriptionFighter = FightersFactory.getClassDescriptions(FighterType.valueOf(fighterType));
        MainActivity.loadingImage(this, cash.getCurrentFighter().getImageUrl(), fighterImage);
        fighterClass.setText(fighterType);
        fighterDescription.setText(descriptionFighter);
    }

    private ArenaFighter createNewFighterParams() {
        ArenaFighter fighter = cash.getCurrentFighter();
        fighter.setName(FightersFactory.generateName());
        fighter.setMaxHealth(FightersFactory.generateValue(FightersFactory.DEFAULT_VALUE_HEALTH));
        fighter.setDamage(FightersFactory.generateValue(FightersFactory.DEFAULT_VALUE_DAMAGE));
        fighter.setArmor(FightersFactory.generatePercentValue());
        return fighter;
    }

    private void displayNewFighterParameters(ArenaFighter fighter) {
        fighterName.setText(fighter.getName());
        fighterHP.setText(getFormattedFloat(fighter.getMaxHealth()));
        fighterDamage.setText(getFormattedFloat(fighter.getDamage()));
        fighterArmor.setText(getFormattedFloat(fighter.getArmor()));
    }

    private void initViews() {
        fighterImage = findViewById(R.id.iv_image_fighter);
        fighterName = findViewById(R.id.tv_value_name);
        fighterClass = findViewById(R.id.tv_class_fighter);
        fighterDescription = findViewById(R.id.tv_description);
        fighterHP = findViewById(R.id.value_hp);
        fighterArmor = findViewById(R.id.value_armor);
        fighterDamage = findViewById(R.id.value_damage);

        Button fightStart = findViewById(R.id.bt_start_fight);
        Button fighterReplace = findViewById(R.id.bt_replace_ch);

        fighterReplace.setOnClickListener(this::onClick);
        fightStart.setOnClickListener(this::onClick);
    }

    private void returnChosenFighter() {
        Intent intent = new Intent(this, ChoiceFighterActivity.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_replace_ch:
                displayNewFighterParameters(createNewFighterParams());
                break;
            case R.id.bt_start_fight:
                returnChosenFighter();
                break;
        }
    }

    public String getFormattedFloat(float number) {
        return getString(R.string.text_format_float, number);
    }
}
