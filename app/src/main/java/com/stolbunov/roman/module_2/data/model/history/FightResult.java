package com.stolbunov.roman.module_2.data.model.history;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;

public class FightResult {
    private ArenaFighter winnerBattle;

    public FightResult(ArenaFighter winnerBattle) {
        this.winnerBattle = winnerBattle;
    }

    public ArenaFighter getWinnerBattle() {
        return winnerBattle;
    }
}
