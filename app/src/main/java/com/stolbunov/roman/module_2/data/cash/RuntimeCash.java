package com.stolbunov.roman.module_2.data.cash;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.history.FightHistory;

import java.util.ArrayList;
import java.util.List;


public class RuntimeCash {
    private final int LIST_CAPACITY = 25;

    private ArenaFighter currentFighter;
    private List<FightHistory> histories = new ArrayList<>(LIST_CAPACITY);

    private static RuntimeCash runtimeCash;

    private RuntimeCash() {
    }

    public static RuntimeCash getInstance() {
        if (runtimeCash == null) {
            runtimeCash = new RuntimeCash();
        }

        return runtimeCash;
    }

    public void addNewFightHistory(FightHistory history) {
        histories.add(history);
    }

    public FightHistory getFightHistoryOnID(int id) {
        return histories.get(id);
    }

    public FightHistory getLastFightHistory() {
        return histories.get(histories.size() - 1);
    }

    public List<FightHistory> getHistories() {
        return histories;
    }

    public void setCurrentFighter(ArenaFighter currentFighter) {
        this.currentFighter = currentFighter;
    }

    public ArenaFighter getCurrentFighter() {
        return currentFighter;
    }

    public int getNumberCompletedBattles() {
        return histories.size();
    }
}
