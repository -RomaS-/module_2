package com.stolbunov.roman.module_2.bl.arenas;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;

public class DuelArenaFactory {
    public static BattleArena create(ArenaFighter fighter1, ArenaFighter fighter2) {
        return new DuelArena(null, fighter1, fighter2, 5);
    }
}
