package com.stolbunov.roman.module_2.bl.arenas;

import com.stolbunov.roman.module_2.data.model.history.FightTurn;

public interface FightTurnCallback {
    void publishTurn(FightTurn turn);
}
