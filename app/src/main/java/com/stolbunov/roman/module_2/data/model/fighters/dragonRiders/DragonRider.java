package com.stolbunov.roman.module_2.data.model.fighters.dragonRiders;


import android.os.Parcel;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FighterType;
import com.stolbunov.roman.module_2.data.model.fighters.dragons.Dragon;

public class DragonRider extends ArenaFighter {
    private Dragon ridingDragon;
    private boolean enemyDragon;

    public DragonRider(String name, float health, float damage, float armor, String url) {
        super(name, health, damage, armor, url);
    }

    public DragonRider(Parcel in) {
        super(in);
        this.ridingDragon = in.readParcelable(Dragon.class.getClassLoader());
    }

    @Override
    public float attack(ArenaFighter var1) {
        if (var1 instanceof Dragon) {
            enemyDragon = true;
            attackDragon((Dragon) var1);
            return 0f;
        } else {
            enemyDragon = false;
            return var1.damaged(this.damage);
        }
    }

    private void attackDragon(Dragon dragon) {
        ridingDragon = dragon;
        health += dragon.getHealth();
        maxHealth += dragon.getMaxHealth();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(ridingDragon, flags);
    }

    @Override
    protected int getType() {
        return FighterType.DRAGON_RIDER.getType();
    }

    @Override
    public FighterType getClassFighter() {
        return FighterType.DRAGON_RIDER;
    }

    @Override
    public String getFighterClass() {
        return FighterType.DRAGON_RIDER.name();
    }

    @Override
    public boolean isStopFight() {
        return enemyDragon;
    }
}
