package com.stolbunov.roman.module_2.ui.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.module_2.R;
import com.stolbunov.roman.module_2.data.cash.RuntimeCash;
import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FightersFactory;
import com.stolbunov.roman.module_2.data.model.history.FightHistory;

public class MainActivity extends AppCompatActivity {
    public static final int RC_CHOICE_FIGHTER = 159;
    public static final int RC_RESULT_FIGHT = 1523;

    private ImageView profileImage;

    private TextView profileName;
    private TextView profileDescription;

    private TextView currentHp;
    private TextView currentArmor;
    private TextView currentDamage;

    private Button startNewChange;

    private LinearLayout lParentForHistory;
    private HistoryView historyView;

    private View progress;
    private View profileSections;

    private RuntimeCash cash = RuntimeCash.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        restoreState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cash.getCurrentFighter() != null) {
            progress.setVisibility(View.GONE);
            profileSections.setVisibility(View.VISIBLE);
        }
    }

    private void restoreState(Bundle savedInstanceState) {
        if (cash.getCurrentFighter() == null) {
            progress.setVisibility(View.VISIBLE);
            profileSections.setVisibility(View.GONE);
        } else {
            progress.setVisibility(View.GONE);
            profileSections.setVisibility(View.VISIBLE);
            startNewChange.setText(getString(R.string.start_new_chalange));

            addFighterOnUI();
            fillingHistoryCompletedBattles();
        }
    }

    private void fillingHistoryCompletedBattles() {
        for (FightHistory history : cash.getHistories()) {
            initViewForHistory();
            addFightResultHistoryOnLayout(history);
        }
    }

    private void setDataOnUi() {
        loadingImage(this, cash.getCurrentFighter(), profileImage);

        profileName.setText(cash.getCurrentFighter().getName());
        profileDescription.setText(FightersFactory.getClassDescriptions(cash.getCurrentFighter().getClassFighter()));

        currentHp.setText(getFormattedFloat(cash.getCurrentFighter().getMaxHealth()));
        currentDamage.setText(getFormattedFloat(cash.getCurrentFighter().getDamage()));
        currentArmor.setText(getFormattedFloat(cash.getCurrentFighter().getArmor()));
    }


    private void choiceFighter(View view) {
        if (cash.getCurrentFighter() == null) {
            createIntentForResult(RC_CHOICE_FIGHTER, ChoiceFighterActivity.class);
        } else {

            if (cash.getCurrentFighter().getHealth() > 0) {
                createIntentForResult(RC_RESULT_FIGHT, BattleActivity.class);
            } else {
                Toast.makeText(this, R.string.fighter_died, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void createIntentForResult(int requestCode, Class toClass) {
        Intent intent = new Intent(this, toClass);
        startActivityForResult(intent, requestCode);
    }

    private void addFighterOnUI() {
        startNewChange.setText(getString(R.string.start_new_chalange));
        setDataOnUi();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_CHOICE_FIGHTER:
                    addFighterOnUI();
                    break;
                case RC_RESULT_FIGHT:
                    addFightResultHistoryOnLayout(cash.getLastFightHistory());
                    break;
            }
        }
    }

    private void addFightResultHistoryOnLayout(FightHistory history) {
        if (history != null) {
            initViewForHistory();
            historyView.fillingViewHistory(history);
            View view = historyView.getView();
            view.setOnClickListener(this::showBattleDetails);
            view.setId(cash.getNumberCompletedBattles() - 1);
            lParentForHistory.addView(historyView.getView());
        }
    }

    private void showBattleDetails(View view) {
        Intent intent = new Intent(this, BattleActivity.class);
        intent.putExtra(BattleActivity.SHOW_HISTORY_BATTLE, view.getId());
        startActivity(intent);
    }

    private void initViews() {
        profileImage = findViewById(R.id.fighter_icon);

        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        progress = findViewById(R.id.progress_profile);
        profileSections = findViewById(R.id.profile);

        startNewChange = findViewById(R.id.btn_start_new_chalange);
        startNewChange.setOnClickListener(this::choiceFighter);

        lParentForHistory = findViewById(R.id.battle_history);
    }

    private void loadingImage(Context context, ArenaFighter fighter, ImageView view) {
        loadingImage(context, fighter.getImageUrl(), view);
    }

    public static void loadingImage(Context context, String uri, ImageView view) {
        Glide.with(context)
                .load(uri)
                .into(view);
    }

    private void initViewForHistory() {
        LayoutInflater inflater = getLayoutInflater();
        historyView = new HistoryView(inflater, lParentForHistory);
    }

    public String getFormattedFloat(float number) {
        return String.format(getString(R.string.text_format_float), number);
    }
}
