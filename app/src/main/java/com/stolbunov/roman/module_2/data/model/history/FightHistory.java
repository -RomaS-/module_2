package com.stolbunov.roman.module_2.data.model.history;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;

import java.util.LinkedList;
import java.util.List;

public class FightHistory {
    private float startHPFighter;
    private float startHPEnemy;

    private ArenaFighter fighter;
    private ArenaFighter enemy;

    private List<FightTurn> turns;
    private FightResult fightResult;

    public FightHistory(ArenaFighter fighter, ArenaFighter enemy, FightResult fightResult, LinkedList<FightTurn> turns) {
        this.fighter = fighter;
        this.enemy = enemy;
        this.fightResult = fightResult;
        this.turns = turns;
        startHPFighter = turns.get(0).getStartHPFighter();
        startHPEnemy = turns.get(0).getStartHPEnemy();
    }

    public ArenaFighter getFighter() {
        return fighter;
    }

    public ArenaFighter getEnemy() {
        return enemy;
    }

    public List<FightTurn> getTurns() {
        return turns;
    }

    public FightResult getFightResult() {
        return fightResult;
    }

    public float getStartHPFighter() {
        return startHPFighter;
    }

    public float getStartHPEnemy() {
        return startHPEnemy;
    }
}
