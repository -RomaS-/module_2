package com.stolbunov.roman.module_2.data.model.history;

public class FightTurn {
    private float startHPFighter;
    private float endHPFighter;
    private float damageFighter;
    private float startHPEnemy;
    private float endHPEnemy;
    private float damageEnemy;

    public FightTurn(float startHPFighter, float endHPFighter, float damageFighter,
                     float startHPEnemy, float endHPEnemy, float damageEnemy) {
        this.startHPFighter = startHPFighter;
        this.endHPFighter = endHPFighter;
        this.damageFighter = damageFighter;
        this.startHPEnemy = startHPEnemy;
        this.endHPEnemy = endHPEnemy;
        this.damageEnemy = damageEnemy;
    }



    public float getStartHPFighter() {
        return startHPFighter;
    }

    public float getEndHPFighter() {
        return endHPFighter;
    }

    public float getDamageFighter() {
        return damageFighter;
    }

    public float getStartHPEnemy() {
        return startHPEnemy;
    }

    public float getEndHPEnemy() {
        return endHPEnemy;
    }

    public float getDamageEnemy() {
        return damageEnemy;
    }
}
