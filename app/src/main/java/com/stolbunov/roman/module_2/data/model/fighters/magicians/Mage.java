package com.stolbunov.roman.module_2.data.model.fighters.magicians;


import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.Elements;
import com.stolbunov.roman.module_2.data.model.fighters.FighterType;

public class Mage extends ArenaFighter implements Elements {
    int element;

    public Mage (String name, float health, float damage, float armor, int element) {
        super(name, health, damage, armor,null);
        this.element = element;
    }

    @Override
    public float attack (ArenaFighter fighters) {
        if( fighters instanceof Elements ) {
            if( isElementsEquals(((Elements) fighters).getElements()) ) {
                return 0;
            }
        }
        return fighters.damaged(damage);
    }

    @Override
    public String getFighterClass() {
        return null;
    }

    @Override
    public boolean isStopFight() {
        return false;
    }

    @Override
    protected int getType() {
        // todo create type for fighter
        return 0;
    }

    @Override
    public FighterType getClassFighter() {
        // todo create type for fighter
        return null;
    }

    @Override
    public int getElements () {
        return element;
    }
}
