package com.stolbunov.roman.module_2.data.model.fighters;


import android.os.Parcel;
import android.os.Parcelable;

import com.stolbunov.roman.module_2.data.model.fighters.dragonRiders.DragonRider;
import com.stolbunov.roman.module_2.data.model.fighters.dragons.Dragon;
import com.stolbunov.roman.module_2.data.model.fighters.knights.BlackKnight;
import com.stolbunov.roman.module_2.data.model.fighters.knights.HollyKnight;

public abstract class ArenaFighter implements Parcelable {
    protected String name;
    protected float health;
    protected float damage;
    protected float armor;
    protected float maxHealth;
    String imageUrl;
    float totalDamage;

    public ArenaFighter(String name,
                        float health,
                        float damage,
                        float armor,
                        String imageUrl
    ) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.health = health;
        this.damage = damage;
        this.armor = armor;
        this.maxHealth = health;
    }

    public boolean isAlfie() {
        return health > 0;
    }

    public String getName() {
        return this.name;
    }

    public void heal(float health) {
        float newHeal = this.health + health;
        this.health = Math.min(newHeal, this.maxHealth);
    }

    protected abstract int getType();

    public abstract float attack(ArenaFighter var1);

    public abstract String getFighterClass();

    public abstract boolean isStopFight();

    public float damaged(float damageTaken) {
        float resist = (damageTaken * armor);
        totalDamage += resist;
        float gotDamage = damageTaken - resist;
        health -= gotDamage;
        return gotDamage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getType());
        dest.writeString(this.name);
        dest.writeString(this.imageUrl);
        dest.writeFloat(this.health);
        dest.writeFloat(this.damage);
        dest.writeFloat(this.armor);
        dest.writeFloat(this.maxHealth);
    }

    protected ArenaFighter(Parcel in) {
        this.name = in.readString();
        this.imageUrl = in.readString();
        this.health = in.readFloat();
        this.damage = in.readFloat();
        this.armor = in.readFloat();
        this.maxHealth = in.readFloat();
    }

    public static final Creator<ArenaFighter> CREATOR = new Creator<ArenaFighter>() {
        public ArenaFighter createFromParcel(Parcel source) {
            int type = source.readInt();
            switch (type) {
                case 1:
                    return new Dragon(source);
                case 2:
                    return new DragonRider(source);
                case 3:
                    return new HollyKnight(source);
                case 4:
                    return new BlackKnight(source);
            }
            throw new RuntimeException();
        }

        public ArenaFighter[] newArray(int size) {
            return new ArenaFighter[size];
        }
    };

    public void setTotalDamage(float totalDamage) {
        this.totalDamage = totalDamage;
    }

    public float getTotalDamage() {
        return totalDamage;
    }

    public abstract FighterType getClassFighter();

    public String getImageUrl() {
        return imageUrl;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public float getHealth() {
        return health;
    }

    public float getDamage() {
        return damage;
    }

    public float getArmor() {
        return armor;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public String getDescription() {
        return "Description";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxHealth(float maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public void setArmor(float armor) {
        this.armor = armor;
    }
}
