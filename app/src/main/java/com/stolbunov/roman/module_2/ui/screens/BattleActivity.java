package com.stolbunov.roman.module_2.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stolbunov.roman.module_2.R;
import com.stolbunov.roman.module_2.bl.arenas.BattleArena;
import com.stolbunov.roman.module_2.bl.arenas.DuelArenaFactory;
import com.stolbunov.roman.module_2.data.cash.RuntimeCash;
import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FighterType;
import com.stolbunov.roman.module_2.data.model.fighters.FightersFactory;
import com.stolbunov.roman.module_2.data.model.history.FightHistory;
import com.stolbunov.roman.module_2.data.model.history.FightResult;
import com.stolbunov.roman.module_2.data.model.history.FightTurn;

import java.util.LinkedList;

public class BattleActivity extends AppCompatActivity {
    private final int NUM_DEFAULT_VALUE = -1;
    private final int ONE_ROUND = 1;
    private final int FINISH_BATTLE = 2;
    private final String CURRENT_ENEMY = "CURRENT_ENEMY";
    public final static String SHOW_HISTORY_BATTLE = "SHOW_HISTORY_BATTLE";

    private ViewGroup parentRounds;
    private Button fightDone;

    private ArenaFighter fighter;
    private ArenaFighter enemy;
    private HistoryView view = new HistoryView();
    private LinkedList<FightTurn> turns = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle);

        Intent intent = getIntent();
        int historyID = intent.getIntExtra(SHOW_HISTORY_BATTLE, NUM_DEFAULT_VALUE);

        if (historyID >= 0) {
            showBattleHistory(historyID);
        } else {
            restoreState(savedInstanceState);
        }
    }

    private void restoreState(Bundle savedInstanceState) {
        fighter = RuntimeCash.getInstance().getCurrentFighter();
        if (savedInstanceState == null) {
            creatingRandomEnemy();
            initViews();
            fillingTitleView();
            new Fight().start();

        } else {
            enemy = savedInstanceState.getParcelable(CURRENT_ENEMY);
            initViews();
            fillingTitleView();

//            addNewFightTurn();
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CURRENT_ENEMY, enemy);
    }

    private void showBattleHistory(int historyID) {
        FightHistory history = RuntimeCash.getInstance().getFightHistoryOnID(historyID);
        fighter = history.getFighter();
        enemy = history.getEnemy();

        initViews();
        fillingTitleView();

        fightDone.setText(R.string.back);
        fightDone.setEnabled(true);

        for (FightTurn turn : history.getTurns()) {
            addNewFightTurn(turn);
        }
    }

    private void initViews() {
        fightDone = findViewById(R.id.btn_fight_done);
        fightDone.setOnClickListener(this::fightDone);

        parentRounds = findViewById(R.id.ll_rounds);

        ViewGroup viewGroup = findViewById(R.id.ll_battle);
        LayoutInflater inflater = getLayoutInflater();
        view.createView(inflater, viewGroup);
        viewGroup.addView(view.getView());
    }

    private void fightDone(View view) {
        CharSequence textButton = fightDone.getText();

        if (textButton.equals(getString(R.string.fight_done))) {
            Intent intent = new Intent(this, MainActivity.class);
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    private void fillingTitleView() {
        view.fillingViewHistory(fighter, enemy);
    }

    private void creatingRandomEnemy() {
        FighterType type = FighterType.randomType();
        enemy = FightersFactory.generateFighter(type);
    }

    private void handleFightTurn(FightTurn turn) {
        turns.add(turn);
        addNewFightTurn(turn);
    }

    private void addNewFightTurn() {
        for (FightTurn turn : turns) {
            addNewFightTurn(turn);
        }
    }

    private void addNewFightTurn(FightTurn turn) {
        LayoutInflater inflater = getLayoutInflater();
        view.createView(inflater, parentRounds);
        view.fillingViewHistory(fighter, enemy, turn);
        parentRounds.addView(view.getView());
    }

    private void finishBattle(ArenaFighter winner) {
        fightDone.setEnabled(true);
        RuntimeCash.getInstance()
                .addNewFightHistory(
                        new FightHistory(fighter, enemy, new FightResult(winner), turns));
    }

    class Fight extends Thread {
        @Override
        public void run() {
            super.run();
            BattleArena arena = DuelArenaFactory.create(fighter, enemy);
            arena.setFightCallback(turn -> handler.sendMessage(Message.obtain(handler, ONE_ROUND, turn)));
            arena.startBattle();
            handler.sendMessage(Message.obtain(handler, FINISH_BATTLE, arena.getWinner()));
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case ONE_ROUND:
                    handleFightTurn((FightTurn) msg.obj);
                    break;
                case FINISH_BATTLE:
                    finishBattle((ArenaFighter) msg.obj);
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
    }
}
