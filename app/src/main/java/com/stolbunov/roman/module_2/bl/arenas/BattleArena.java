package com.stolbunov.roman.module_2.bl.arenas;


import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.PostAttackAction;
import com.stolbunov.roman.module_2.data.model.healers.Healer;
import com.stolbunov.roman.module_2.data.model.history.FightTurn;

public abstract class BattleArena {
    Healer healer;
    GodHand godHand;
    private FightTurnCallback fightTurnCallback;

    BattleArena(Healer healer) {
        this.healer = healer;
    }

    public abstract void startBattle();

    public abstract ArenaFighter getWinner();

    public void setGodHand(GodHand godHand) {
        this.godHand = godHand;
    }

    ArenaFighter calculationOfWinner(ArenaFighter participant1, ArenaFighter participant2) {
        if (participant1 != null && participant2 != null) {
            if (participant1.isAlfie() && participant2.isAlfie()) {
                return (participant1.getHealth() > participant2.getHealth()) ? participant1 : participant2;
            } else if (participant1.isAlfie()) {
                return participant1;
            } else if (participant2.isAlfie()) {
                return participant2;
            }
        }
        return null;
    }

    public void setFightCallback(FightTurnCallback fightTurnCallback) {
        this.fightTurnCallback = fightTurnCallback;
    }

    void confrontation(ArenaFighter participant1, ArenaFighter participant2) {
        float startHPPart1 = participant1.getHealth();
        float startHPPart2 = participant2.getHealth();
        float dam1 = participant1.attack(participant2);
        float dam2 = participant2.attack(participant1);
        if (participant1 instanceof PostAttackAction) {
            ((PostAttackAction) participant1).postAttackAction(dam1, dam2);
        }

        if (participant2 instanceof PostAttackAction) {
            ((PostAttackAction) participant2).postAttackAction(dam2, dam1);
        }

        if (fightTurnCallback != null) {
            fightTurnCallback.publishTurn(new FightTurn(startHPPart1, participant1.getHealth(), dam1,
                    startHPPart2, participant2.getHealth(), dam2));
        }

        if (healer != null) {
            healer.heal(dropTheCoin() ? participant1 : participant2);
        }

        if (godHand != null) {
            godHand.godHand(participant1, participant2);
        }

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    boolean isFightContinue(ArenaFighter participant1, ArenaFighter participant2) {
        return participant1 != null && participant2 != null;
    }

    public interface GodHand {
        boolean godHand(ArenaFighter participant1, ArenaFighter participant2);
    }
}
