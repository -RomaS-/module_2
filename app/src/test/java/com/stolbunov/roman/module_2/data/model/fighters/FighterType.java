package com.stolbunov.roman.module_2.data.model.fighters;

public enum FighterType {
    DRAGON(1), DRAGON_RIDER(2), HOLLY_KNIGHT(3), BLACK_KNIGHT(4), KNIGHT(5);

    int type;

    FighterType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static FighterType randomType() {
        int numEnemy = (int) (Math.random() * 4) + 1;
        switch (numEnemy) {
            case 1:
                return DRAGON;
            case 2:
                return DRAGON_RIDER;
            case 3:
                return HOLLY_KNIGHT;
            case 4:
                return BLACK_KNIGHT;
//            case 5:
//                return KNIGHT;
        }
        return DRAGON;
    }
}
