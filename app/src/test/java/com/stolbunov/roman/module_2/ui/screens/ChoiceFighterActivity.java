package com.stolbunov.roman.module_2.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.stolbunov.roman.module_2.R;
import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FightersFactory;

import static com.stolbunov.roman.module_2.data.model.fighters.FighterType.BLACK_KNIGHT;
import static com.stolbunov.roman.module_2.data.model.fighters.FighterType.DRAGON;
import static com.stolbunov.roman.module_2.data.model.fighters.FighterType.DRAGON_RIDER;
import static com.stolbunov.roman.module_2.data.model.fighters.FighterType.HOLLY_KNIGHT;

public class ChoiceFighterActivity extends AppCompatActivity {
    private final int RC_SELECTED_FIGHTER = 654;
    public static final String SELECTED_FIGHTER = "SELECTED_FIGHTER";

    ImageView image1;
    ImageView image2;
    ImageView image3;
    ImageView image4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_enemy);

        image1 = findViewById(R.id.iv_choice_fighter1);
        image2 = findViewById(R.id.iv_choice_fighter2);
        image3 = findViewById(R.id.iv_choice_fighter3);
        image4 = findViewById(R.id.iv_choice_fighter4);

        image1.setOnClickListener(this::choiceFighter);
        image2.setOnClickListener(this::choiceFighter);
        image3.setOnClickListener(this::choiceFighter);
        image4.setOnClickListener(this::choiceFighter);

        loadingImage();
    }

    private void choiceFighter(View view) {
        Intent intent = new Intent(this, ChoiceFighterCharacteristicsActivity.class);
        String type;
        int viewID = getViewID(view);
        switch (viewID) {
            case R.id.iv_battle_fighter1:
                type = DRAGON_RIDER.name();
                break;
            case R.id.iv_battle_fighter2:
                type = DRAGON.name();
                break;
            case R.id.iv_choice_fighter3:
                type = HOLLY_KNIGHT.name();
                break;
            case R.id.iv_choice_fighter4:
                type = BLACK_KNIGHT.name();
                break;
                default:
                    type = DRAGON.name();
        }
        intent.putExtra(SELECTED_FIGHTER, type);
        startActivityForResult(intent, RC_SELECTED_FIGHTER);
    }

    private int getViewID(View view) {
        long id = view.getId();
        if (id == 2131165270 || id == 2131165269) {
            return (int) (id - 3);
        } else {
            return (int) id;
        }
    }

    private void loadingImage() {
        MainActivity.loadingImage(this,
                FightersFactory.getFighterImageURL(DRAGON_RIDER),
                image1);

        MainActivity.loadingImage(this,
                FightersFactory.getFighterImageURL(DRAGON),
                image2);

        MainActivity.loadingImage(this,
                FightersFactory.getFighterImageURL(HOLLY_KNIGHT),
                image3);

        MainActivity.loadingImage(this,
                FightersFactory.getFighterImageURL(BLACK_KNIGHT),
                image4);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_SELECTED_FIGHTER:
                    returnChosenFighter(data.getParcelableExtra(SELECTED_FIGHTER));
                    break;
            }
        }
    }

    void returnChosenFighter(ArenaFighter fighter) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(SELECTED_FIGHTER, fighter);
        setResult(RESULT_OK, intent);
        finish();
    }
}
