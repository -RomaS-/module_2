package com.stolbunov.roman.module_2.data.cash;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;


public class RuntimeCash {
    private static RuntimeCash runtimeCash;

    private RuntimeCash() {}

    public static RuntimeCash getInstance() {
        if (runtimeCash == null)
            runtimeCash = new RuntimeCash();

        return runtimeCash;
    }

    private ArenaFighter currentFighter;

    public void setCurrentFighter(ArenaFighter currentFighter) {
        this.currentFighter = currentFighter;
    }

    public ArenaFighter getCurrentFighter() {
        return currentFighter;
    }
}
