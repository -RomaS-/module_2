package com.stolbunov.roman.module_2.bl.arenas;


import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.healers.Healer;

public class DuelArena extends BattleArena {
    private ArenaFighter participant1;
    private ArenaFighter participant2;
    private int roundCountMax;


    DuelArena(Healer healer, ArenaFighter participant1, ArenaFighter participant2, int roundCountMax) {
        super(healer);
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.roundCountMax = roundCountMax;
        resetDamageCounter(participant1);
        resetDamageCounter(participant2);
    }

    @Override
    public void startBattle() {
        int currentRound = 0;
        while (currentRound < roundCountMax && isFightContinue(participant1, participant2)) {
            confrontation(participant1, participant2);
            currentRound++;
        }
    }

    @Override
    public ArenaFighter getWinner() {
        ArenaFighter winners = calculationOfWinner(participant1, participant2);
        if (winners != null) {
            return winners;
        } else {
            return null;
        }
    }

    private void resetDamageCounter(ArenaFighter fighter) {
        fighter.setTotalDamage(0);
    }
}
