package com.stolbunov.roman.module_2.data.model.fighters.knights;


import android.os.Parcel;

import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.fighters.FighterType;

import java.util.Random;

public class Knight extends ArenaFighter {
    private float shield;

    Knight(String name, float health, float damage, float armor, float shield, String imageUrl) {
        super(name, health, damage, armor, imageUrl);
        this.shield = shield;
    }

    Knight(Parcel in) {
        super(in);
        shield = in.readFloat();
    }

    public float attack(ArenaFighter arenaFighter) {
        return arenaFighter.damaged(this.damage);
    }

    @Override
    public String getFighterClass() {
        return FighterType.KNIGHT.name();
    }

    @Override
    public boolean isStopFight() {
        return false;
    }

    public float damaged(float damageTaken) {
        Random random = new Random();
        if (random.nextGaussian() > (double) this.shield) {
            return super.damaged(damageTaken);
        } else {
            System.out.println(this.name + " blocked");
        }
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(shield);
    }

    @Override
    protected int getType() {
        return FighterType.KNIGHT.getType();
    }

    @Override
    public FighterType getClassFighter() {
        return FighterType.KNIGHT;
    }
}
