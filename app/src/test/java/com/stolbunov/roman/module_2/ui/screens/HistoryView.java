package com.stolbunov.roman.module_2.ui.screens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.module_2.R;
import com.stolbunov.roman.module_2.data.model.fighters.ArenaFighter;
import com.stolbunov.roman.module_2.data.model.history.FightHistory;
import com.stolbunov.roman.module_2.data.model.history.FightTurn;

public class HistoryView {
    private static final String URI_VS_IMAGE =
            "http://static1.comicvine.com/uploads/original/11112/111129141/5440487-1122329314-52705.png";

    private Context context;
    private ImageView battleImageFighter1;
    private ImageView battleImageFighter2;
    private ImageView battleImageVS;
    private TextView battleNameFighter1;
    private TextView battleNameFighter2;
    private TextView battleHPFighter1;
    private TextView battleHPFighter2;
    private TextView battleResult;
    private View battleView;

    HistoryView() {
    }

    HistoryView(LayoutInflater inflater, ViewGroup parent) {
        createView(inflater, parent);
    }

    public void createView(LayoutInflater inflater, ViewGroup parent) {
        context = inflater.getContext();
        battleView = inflater.inflate(R.layout.showing_opponents, parent, false);

        battleImageFighter1 = battleView.findViewById(R.id.iv_battle_fighter1);
        battleImageFighter2 = battleView.findViewById(R.id.iv_battle_fighter2);
        battleImageVS = battleView.findViewById(R.id.iv_battle_vs);
        battleNameFighter1 = battleView.findViewById(R.id.tv_name_fighter1);
        battleNameFighter2 = battleView.findViewById(R.id.tv_name_fighter2);
        battleHPFighter1 = battleView.findViewById(R.id.tv_hp_fighter1);
        battleHPFighter2 = battleView.findViewById(R.id.tv_hp_fighter2);
        battleResult = battleView.findViewById(R.id.tv_battle_result);
        changeVisibility(View.GONE);
    }

    public void fillingViewHistory(ArenaFighter currentFighter, ArenaFighter currentEnemy, FightTurn turn) {
        fillingViewHistory(currentFighter, currentEnemy);
        changeVisibility(View.VISIBLE);
        battleHPFighter1.setText(getFormattedFloat(turn.getStartHPFighter()));
        battleHPFighter2.setText(getFormattedFloat(turn.getEndHPEnemy()));
        battleResult.setText(String.format(
                context.getResources().getString(R.string.text_one_round),
                currentFighter.getName(), turn.getDamageFighter(), currentEnemy.getName(), turn.getDamageEnemy(),
                currentFighter.getName(), turn.getEndHPFighter(), currentEnemy.getName(), turn.getEndHPEnemy()));
    }

    public void fillingViewHistory(ArenaFighter currentFighter, ArenaFighter currentEnemy) {
        MainActivity.loadingImage(context, currentFighter.getImageUrl(), battleImageFighter1);
        MainActivity.loadingImage(context, currentEnemy.getImageUrl(), battleImageFighter2);
        MainActivity.loadingImage(context, URI_VS_IMAGE, battleImageVS);
        battleNameFighter1.setText(currentFighter.getName());
        battleNameFighter2.setText(currentEnemy.getName());
    }

    public void fillingViewHistory(FightHistory history) {
        ArenaFighter winner = history.getFightResult().getWinnerBattle();

        fillingViewHistory(history.getFighter(), history.getEnemy());
        changeVisibility(View.VISIBLE);
        battleHPFighter1.setText(getFormattedFloat(history.getStartHPFighter()));
        battleHPFighter2.setText(getFormattedFloat(history.getStartHPEnemy()));
        battleResult.setText(String.format(
                context.getResources().getString(R.string.battle_result),
                winner.getName(),
                winner.getTotalDamage(),
                winner.getHealth()));
    }

    public View getView() {
        return battleView;
    }

    private void changeVisibility(int visibility) {
        battleHPFighter1.setVisibility(visibility);
        battleHPFighter2.setVisibility(visibility);
        battleResult.setVisibility(visibility);
    }

    private String getFormattedFloat(float number) {
        return String.format(context.getString(R.string.text_format_float), number);
    }
}
