package com.stolbunov.roman.module_2.data.model.fighters;

public interface PostAttackAction {
    void postAttackAction(float damageTaken, float damageGotten);
}
