package com.stolbunov.roman.module_2.data.model.fighters.knights;

import android.os.Parcel;

import com.stolbunov.roman.module_2.data.model.fighters.FighterType;

public class HollyKnight extends Knight {
    private float recovery;

    public HollyKnight(String name, float health, float damage, float armor, float shield,
                       float recovery, String imageUrl) {
        super(name, health, damage, armor, shield, imageUrl);
        this.recovery = recovery;
    }

    public HollyKnight(Parcel in) {
        super(in);
        recovery = in.readFloat();
    }

    private void recovery() {
        if (this.isAlfie()) {
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(recovery);
    }

    @Override
    public String getFighterClass() {
        return FighterType.HOLLY_KNIGHT.name();
    }

    @Override
    protected int getType() {
        return FighterType.HOLLY_KNIGHT.getType();
    }

    @Override
    public FighterType getClassFighter() {
        return FighterType.HOLLY_KNIGHT;
    }
}